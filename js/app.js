  // Dribbble START
  var accessToken = 'bd84ed8ceb7d20104fc11271963a5cae6ec8079af1aee9e85d1a698124482ac2';
  $.ajax({
      url: 'https://api.dribbble.com/v2/user/shots?access_token='+accessToken,
      dataType: 'json',
      type: 'GET',
      success: function(data) {  
        if (data.length > 0) { 
          $.each(data.reverse(), function(i, val) {                
            $('#dribbble').prepend(
              '<div class="col-tp-50"><a target="_blank" href="'+ val.html_url +'"><div class="card_dribbble" style="background-image:url('+ val.images.hidpi +');"></div></a></div>'
              )
          })
        }
        else {
          console.log("Dribbble not working...")
        }
      }
  });  
  
  $(".text").typed({
    strings: ["I use my design expertise to constantly craft and improve your digital products.<br>I believe in human-centered design."],
    typeSpeed: 0
  });